const express = require('express');
const morgan = require('morgan');

const app = express();

// Middleware für Logging
app.use(morgan('combined'));

// Middleware für zentrale Fehlerbehandlung
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ message: 'Ein interner Serverfehler ist aufgetreten.' });
});

// Routen
app.get('/', (req, res) => {
  res.json({ message: 'Willkommen auf der Startseite!' });
});

app.get('/info', (req, res) => {
  res.json({ message: 'Weitere Details der Anfrage die geloggt werden sind: Antwortzeit in ms, die Länge der Antwort, Remoteadresse und User, Zeitpunkt, HTTP-Version, Referrer und Agent' });
});

app.get('/data', (req, res) => {
  res.json({ message: 'Hier ist die Daten-API!' });
});

app.post('/data', (req, res) => {
  res.json({ message: 'Daten wurden erfolgreich erstellt!' });
});

app.put('/data', (req, res) => {
  res.json({ message: 'Daten wurden erfolgreich aktualisiert!' });
});

app.delete('/data', (req, res) => {
  res.json({ message: 'Daten wurden erfolgreich gelöscht!' });
});

// Starten des Servers
const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server läuft auf http://localhost:${PORT}`);
});